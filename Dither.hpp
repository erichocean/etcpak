#ifndef __DITHER_HPP__
#define __DITHER_HPP__

#include "Types.hpp"

void InitDither();
void Dither( uint8* data );

#endif
