#ifndef __PROCESSRGB_HPP__
#define __PROCESSRGB_HPP__

#include "Types.hpp"

uint64 ProcessRGB( const uint8* src );

#endif
