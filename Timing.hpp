#ifndef __DARKRL__TIMING_HPP__
#define __DARKRL__TIMING_HPP__

#include "Types.hpp"

void InitTiming();
uint64 GetTime();

#endif
